# Lisp C

Little lisp interpreter in C

Why *little* ?

Simply because he support only basic mathematical operations with int64_t and float64_t:

* addition
* subtraction
* multplication
* division
* modulo (Only for integer and with two operands)

More safe treatment of numbers are coming soon.

**NB : I have limited number of char gets to 255 and because I use C lang we have a limitation for
number treatment.(Simply don't over 2.225074e-308(negative values) and 1.797693e+308(positive values))**