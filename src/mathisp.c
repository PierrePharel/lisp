#include <stdlib.h>
#include <lisp.h>
#include <mathisp.h>
uint8_t n_whspace_char = 0;
char *end;

float64_t computer_add(char *expr)
{
	float64_t sum = 0;

	check_n_whspace_char(expr, &n_whspace_char);
	for(uint8_t i = 0; i < n_whspace_char; ++ i)
	{
		float64_t m;

		expr = to_whitespace(expr);
		m = strtof(expr++, &end);
		sum += m;
	}
	return sum;
}
float64_t computer_product(char *expr)
{
	float64_t product = 1;

	check_n_whspace_char(expr, &n_whspace_char);
	for(uint8_t i = 0; i < n_whspace_char; ++ i)
	{
		float64_t m;

		expr = to_whitespace(expr);
		m = strtof(expr++, &end);
		product *= m;
	}
	return product;
}
float64_t computer_div(char *expr)
{
	float64_t quotient;

	check_n_whspace_char(expr, &n_whspace_char);
	for(uint8_t i = 0; i < n_whspace_char; ++ i)
	{
		float64_t m;

		expr = to_whitespace(expr);
		m = strtof(expr++, &end);
		if(i < 1)
			quotient = m;
		else
			quotient /= m;
	}
	return quotient;
}
float64_t computer_sub(char *expr)
{
	float64_t difference;

	check_n_whspace_char(expr, &n_whspace_char);
	for(uint8_t i = 0; i < n_whspace_char; ++ i)
	{
		float64_t m;

		expr = to_whitespace(expr);
		m = strtof(expr++, &end);
		if(i < 1)
			difference = m;
		else
			difference -= m;
	}
	return difference;	
}
int64_t computer_mod(char *expr)
{
	int64_t m;
	int64_t n;
	char *buff;

	buff = to_whitespace(expr);
	m = atol(buff);
	n = atol(to_whitespace(++buff));
	return m % n;
}