#include <stdio.h>
#include <stdlib.h> 
#include <lisp.h>

int main(void)
{
	char *expr;

	while(1)
	{
		printf("Enter lisp expression please : ");
		expr = read();
		print(eval(expr));
	}
	return 0;
}