#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <mathisp.h>
#include <lisp.h>

float64_t eval(char *expr)
{
	float64_t result = 0;
	char *op; 
	char *buff = expr;

	do
	{
		buff = ++buff;
	}while(*(buff) == ' ');
	op = buff;
	switch(*op)
	{
		case OP_ADD:
		result = computer_add(expr);
		break;
		case OP_SUB:
		result = computer_sub(expr);
		break;
		case OP_MUL:
		result = computer_product(expr);
		break;
		case OP_DIV:
		result = computer_div(expr);
		break;
		case OP_MOD:
		result = computer_mod(expr);
		break;
		default:
		fprintf(stderr, "Undefined operation.\n");
		result = 0; 
		break;
	}
	return result;
}
int8_t expr_checker(char *expr)
{

	int8_t pchar = 0;

	for(uint8_t i = 0; expr[i] != '\0'; ++ i)
	{
		if(expr[i] == '(' || expr[i] == ')')
			++ pchar;
	}
	if(pchar < 2 || pchar > 2)
	{
		fprintf(stderr, "Expression error : expr must begin with a '(' and finish with ')'.\n");
		return 1;
	}
	return 0;
}
void check_n_whspace_char(char *expr, uint8_t *n_whspace_char)
{
	uint8_t i = 0;

	*n_whspace_char = 0;
	while(expr[i] != '\0')
	{
		if(expr[i] == ' '){++ *n_whspace_char;};
		++ i;
	}
}
char *read(void)
{
	char *expr = malloc(256);
	fgets(expr, 256, stdin);
	expr_checker(expr);
	return expr;
}
char *to_whitespace(char *expr)
{
	return strchr(expr, ' ');
}
void print(float64_t ev_result)
{
	int64_t ev_result_trunc = (int64_t)ev_result;

	if(ev_result == ev_result_trunc)
		printf("-> %ld\n", ev_result_trunc);
	else
		printf("-> %f\n", ev_result);
}