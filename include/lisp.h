#ifndef LISP_H
#define LISP_H
#include <stdint.h>
#include <mathisp.h>
extern int8_t expr_checker(char *expr);
extern void check_n_whspace_char(char *expr, uint8_t *n_whspace_char);
extern char *read(void);
extern char *to_whitespace(char *expr);
extern float64_t eval(char *expr);
extern void print(float64_t ev_result);
#endif