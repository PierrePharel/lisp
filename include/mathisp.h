#ifndef MATHISP_H
#define MATHISP_H
#define OP_ADD ('+')
#define OP_SUB ('-')
#define OP_MUL ('*')
#define OP_DIV ('/')
#define OP_MOD ('%')
#include <stdint.h>
typedef float float32_t; 
typedef double float64_t; 
extern float64_t computer_add(char *expr);
extern float64_t computer_product(char *expr);
extern float64_t computer_div(char *expr);
extern float64_t computer_sub(char *expr);
extern int64_t computer_mod(char *expr);
#endif